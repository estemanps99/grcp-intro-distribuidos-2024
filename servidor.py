import grpc
import os
from concurrent import futures
import estudiantes_pb2
import estudiantes_pb2_grpc

# Datos de estudiantes
estudiantes_data = {
    "334": {"nombre": "Aguilar,María", "grupo": "G3", "taller1": 5, "taller2": 5},
    "444": {"nombre": "Rodriguez, José", "grupo": "G5", "taller1": 4, "taller2": 3},
    "111": {"nombre": "Blanco, Alexandra", "grupo": "G6", "taller1": 3, "taller2": 4},
    "123": {"nombre": "Bolivar, Elizabeth", "grupo": "G3", "taller1": 2.5, "taller2": 5},
    "102": {"nombre": "Burgos, Daniel", "grupo": "G6", "taller1": 5, "taller2": 5},
    "400": {"nombre": "Castro, Gladys", "grupo": "G5", "taller1": 4, "taller2": 4},
    "241": {"nombre": "Rincón, Juan", "grupo": "G4", "taller1": 2, "taller2": 4},
    "231": {"nombre": "Herrera, Carolina", "grupo": "G8", "taller1": 2, "taller2": 2},
    "222": {"nombre": "Dominguez, Marianna", "grupo": "G1", "taller1": 2.9, "taller2": 5},
    "456": {"nombre": "Morales, Santiago", "grupo": "G7", "taller1": 3.4, "taller2": 4.2},
    "665": {"nombre": "Marrero, Alejandro", "grupo": "G6", "taller1": 3.4, "taller2": 4.1},
    "126": {"nombre": "Meneses, Enrique", "grupo": "G7", "taller1": 5, "taller2": 4.7},
    "870": {"nombre": "Rodriguez,Andres", "grupo": "G8", "taller1": 5, "taller2": 4.8},    
    "45": {"nombre": "Sanchez, Liliana", "grupo": "G3", "taller1": 4, "taller2": 5},
    "2": {"nombre": "Soto, Diego", "grupo": "G2", "taller1": 4, "taller2": 3.8},
    "887": {"nombre": "Tineo, Luca", "grupo": "G4", "taller1": 3, "taller2": 3.8},
    "665": {"nombre": "Rosales,Juan Daniel", "grupo": "G4", "taller1": 3.8, "taller2": 4.8},
    "889": {"nombre": "VARGAS, Teresa", "grupo": "G2", "taller1": 3.8, "taller2": 4.7},
    "990": {"nombre": "Vasquez Sanchez,Santiago", "grupo": "G8", "taller1": 4.8, "taller2": 4.5},
    "997": {"nombre": "Vera, Lilia", "grupo": "G2", "taller1": 4.7, "taller2": 5},
    "995": {"nombre": "Cabrales, Elisa", "grupo": "G7", "taller1": 4.5, "taller2": 5},
}



class EstudiantesServicer(estudiantes_pb2_grpc.EstudiantesServiceServicer):
    def GetNombreCompletoByID(self, request, context):
        student_id = request.id
        if student_id in estudiantes_data:
            return estudiantes_pb2.NombreCompleto(nombre_completo=estudiantes_data[student_id]["nombre"])
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details("Estudiante no encontrado")
            return estudiantes_pb2.NombreCompleto()

    def GetPromedioByIDOrNombre(self, request, context):
        id_or_nombre = request.id_or_nombre
        if id_or_nombre in estudiantes_data:
            promedio = (estudiantes_data[id_or_nombre]["taller1"] + estudiantes_data[id_or_nombre]["taller2"]) / 2
            return estudiantes_pb2.Promedio(promedio=promedio)
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details("Estudiante no encontrado")
            return estudiantes_pb2.Promedio()

    def GetGrupoByID(self, request, context):
        student_id = request.id
        if student_id in estudiantes_data:
            return estudiantes_pb2.Grupo(grupo=estudiantes_data[student_id]["grupo"])
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details("Estudiante no encontrado")
            return estudiantes_pb2.Grupo()

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    estudiantes_pb2_grpc.add_EstudiantesServiceServicer_to_server(EstudiantesServicer(), server)
    open_port = os.environ.get("PORT", "50051")
    server.add_insecure_port(f'[::]:{open_port}')
    server.start()
    print("Servidor en ejecución en el puerto 50051")
    server.wait_for_termination()

if __name__ == '__main__':
    serve()
