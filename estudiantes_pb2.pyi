from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Optional as _Optional

DESCRIPTOR: _descriptor.FileDescriptor

class EstudianteID(_message.Message):
    __slots__ = ("id",)
    ID_FIELD_NUMBER: _ClassVar[int]
    id: str
    def __init__(self, id: _Optional[str] = ...) -> None: ...

class EstudianteIDOrNombre(_message.Message):
    __slots__ = ("id_or_nombre",)
    ID_OR_NOMBRE_FIELD_NUMBER: _ClassVar[int]
    id_or_nombre: str
    def __init__(self, id_or_nombre: _Optional[str] = ...) -> None: ...

class NombreCompleto(_message.Message):
    __slots__ = ("nombre_completo",)
    NOMBRE_COMPLETO_FIELD_NUMBER: _ClassVar[int]
    nombre_completo: str
    def __init__(self, nombre_completo: _Optional[str] = ...) -> None: ...

class Promedio(_message.Message):
    __slots__ = ("promedio",)
    PROMEDIO_FIELD_NUMBER: _ClassVar[int]
    promedio: float
    def __init__(self, promedio: _Optional[float] = ...) -> None: ...

class Grupo(_message.Message):
    __slots__ = ("grupo",)
    GRUPO_FIELD_NUMBER: _ClassVar[int]
    grupo: str
    def __init__(self, grupo: _Optional[str] = ...) -> None: ...
