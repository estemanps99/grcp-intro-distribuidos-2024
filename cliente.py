import grpc
import estudiantes_pb2
import estudiantes_pb2_grpc

def run():
    channel = grpc.insecure_channel('localhost:50051')
    stub = estudiantes_pb2_grpc.EstudiantesServiceStub(channel)

    # Ejemplo de uso de los métodos remotos
    # Obtener el nombre completo por ID
    response_nombre = stub.GetNombreCompletoByID(estudiantes_pb2.EstudianteID(id="123"))
    print(f"Nombre completo: {response_nombre.nombre_completo}")

    # Obtener el promedio por ID o nombre
    response_promedio = stub.GetPromedioByIDOrNombre(estudiantes_pb2.EstudianteIDOrNombre(id_or_nombre="123"))
    print(f"Promedio: {response_promedio.promedio}")

    # Obtener el grupo por ID
    response_grupo = stub.GetGrupoByID(estudiantes_pb2.EstudianteID(id="123"))
    print(f"Grupo: {response_grupo.grupo}")

if __name__ == '__main__':
    run()
